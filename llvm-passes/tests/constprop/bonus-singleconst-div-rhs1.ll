define i32 @main(i32 %param.argc, i8** %argv) {
entry:
  %argc = alloca i32
  store i32 %param.argc, i32* %argc
  %x = alloca i32
  %argc.1 = load i32, i32* %argc
  store i32 %argc.1, i32* %x
  %x.1 = load i32, i32* %x
  %x.2 = sdiv i32 %x.1, 1
  ; CHECK: ret i32 %x.1
  ret i32 %x.2
}