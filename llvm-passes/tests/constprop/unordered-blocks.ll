define i32 @main() {
entry:
 br label %actual_entry

end:
  %y = add i32 2, %x
  ; CHECK: ret i32 11
  ret i32 %y

actual_entry:
  %x = add i32 4, 5
  br label %end
}
