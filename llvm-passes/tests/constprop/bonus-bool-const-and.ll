; ModuleID = '<string>'
source_filename = "<string>"
target triple = "aarch64-unknown-linux-gnu"

define void @f() {
entry:
; CHECK: %x = alloca i1
  %x = alloca i1
  %a = and i1 true, true
; CHECK: store volatile i1 true, i1* %x
  store volatile i1 %a, i1* %x
  %b = and i1 true, false
; CHECK: store volatile i1 false, i1* %x
  store volatile i1 %b, i1* %x
  %c = and i1 false, true
; CHECK: store volatile i1 false, i1* %x
  store volatile i1 %c, i1* %x
  %d = and i1 false, false
; CHECK: store volatile i1 false, i1* %x
  store volatile i1 %d, i1* %x
  ret void
}
