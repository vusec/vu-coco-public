define i32 @main(i32 %param.argc, i8** %argv) {
entry:
  br label %entry.while.cond

entry.while.cond:                                 ; preds = %entry.inc, %entry
  %i.0 = phi i32 [ 0, %entry ], [ %.19, %entry.inc ]
  %.8 = icmp slt i32 %i.0, 2
  ; CHECK: %.10 = add i32 2, 2
  ; CHECK: %.12 = add i32 %.10, 5
  ; CHECK: %.14 = add i32 %.12, 1
  ; CHECK: %.16 = add i32 %.14, 2
  ; CHECK: br i1 %.8, label %entry.while.body, label %entry.endwhile
  br i1 %.8, label %entry.while.body, label %entry.endwhile

entry.while.body:                                 ; preds = %entry.while.cond
  ; CHECK-NOT: %.10 = add i32 2, 2
  %.10 = add i32 2, 2
  ; CHECK-NOT: %.12 = add i32 %.10, 5
  %.12 = add i32 %.10, 5
  ; CHECK-NOT: %.14 = add i32 %.12, 1
  %.14 = add i32 %.12, 1
  ; CHECK-NOT: %.16 = add i32 %.14, 2
  %.16 = add i32 %.14, 2
  ; CHECK: br label %entry.inc
  br label %entry.inc

entry.inc:                                        ; preds = %entry.while.body
  %.19 = add i32 %i.0, 1
  br label %entry.while.cond

entry.endwhile:                                   ; preds = %entry.while.cond
  ret i32 0
}