define i32 @main() {
entry:
  ; CHECK: %x = add i32 5, 4
  br label %entry.while.cond

entry.while.cond:                                 ; preds = %entry.while.body.endwhile, %entry
  %i.0 = phi i32 [ 1, %entry ], [ %.16, %entry.while.body.endwhile ]
  %.6 = icmp slt i32 %i.0, 10
  br i1 %.6, label %entry.while.body, label %entry.endwhile

entry.while.body:                                 ; preds = %entry.while.cond
  br label %entry.while.body.while.cond

entry.while.body.while.cond:                      ; preds = %entry.while.body.while.body, %entry.while.body
  %i.2.0 = phi i32 [ 1, %entry.while.body ], [ %.12, %entry.while.body.while.body ]
  %.10 = icmp slt i32 %i.2.0, 20
  br i1 %.10, label %entry.while.body.while.body, label %entry.while.body.endwhile

entry.while.body.while.body:                      ; preds = %entry.while.body.while.cond
  ; CHECK: %.12 = add i32 %i.2.0, 1
  %.12 = add i32 %i.2.0, 1
  ; CHECK-NOT: %x = add i32 5, 4
  %x = add i32 5, 4
  br label %entry.while.body.while.cond

entry.while.body.endwhile:                        ; preds = %entry.while.body.while.cond
  %.16 = add i32 %i.0, 1
  br label %entry.while.cond

entry.endwhile:                                   ; preds = %entry.while.cond
  ret i32 0
}
