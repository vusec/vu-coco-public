; CHECK: @f
define void @f(i1 %cond1, i1 %cond2, i32 %s1, i32 %s2, i32 %s3, i32 %s4, i32 %off) {
; CHECK: %a = alloca i32, i32 %s1
; CHECK: %b = alloca i32, i32 %s2
; CHECK: %c = alloca i32, i32 %s3
  %a = alloca i32, i32 %s1
  %b = alloca i32, i32 %s2
  %c = alloca i32, i32 %s3
  %d = alloca i32, i32 %s4
  br i1 %cond1, label %lbl.if, label %lbl.else

lbl.if:
  br i1 %cond2, label %lbl.if2, label %lbl.exit
lbl.else:
  br i1 %cond2, label %lbl.if2, label %lbl.exit
; CHECK: lbl.if2
lbl.if2:
; CHECK-DAG: %t = phi i32* [ %c, %lbl.if ], [ %d, %lbl.else ]
; CHECK-DAG: [[PHI1:%.*]] = phi i32 [ %s3, %lbl.if ], [ %s4, %lbl.else ]
  %t = phi i32* [ %c, %lbl.if ], [ %d, %lbl.else ]
; CHECK: br label %lbl.exit
  br label %lbl.exit

; CHECK: lbl.exit
lbl.exit:
; CHECK-DAG: %arr = phi i32* [ %a, %lbl.if ], [ %b, %lbl.else ], [ %t, %lbl.if2 ]
; CHECK-DAG: [[PHI2:%.*]] = phi i32 [ %s1, %lbl.if ], [ %s2, %lbl.else ], [ [[PHI1]], %lbl.if2 ]
  %arr = phi i32* [ %a, %lbl.if ], [ %b, %lbl.else ], [ %t, %lbl.if2 ]

; CHECK: call void @__coco_check_bounds({{.*}}i32 %off, i32 [[PHI2]]{{.*}})
; CHECK: %gep = getelementptr i32, i32* %arr, i32 %off
  %gep = getelementptr i32, i32* %arr, i32 %off
  store i32 1, i32* %gep
  ret void
}


