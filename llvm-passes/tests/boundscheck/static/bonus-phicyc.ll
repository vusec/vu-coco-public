; CHECK: @f
define void @f(i1 %cond1, i1 %cond2, i32 %s1, i32 %s2, i32 %off) {
lbl.entry:
; CHECK: %a = alloca i32, i32 %s1
; CHECK: %b = alloca i32, i32 %s2
  %a = alloca i32, i32 %s1
  %b = alloca i32, i32 %s2
  br i1 %cond1, label %lbl.loop, label %lbl.loop.if

; CHECK: lbl.loop:
lbl.loop:
; CHECK-DAG: %pn1 = phi i32* [ %a, %lbl.entry ], [ %pn2, %lbl.loop.if ]
; CHECK-DAG: [[PHI1:%.*]] = phi i32 [ %s1, %lbl.entry ], [ %{{.*}}, %lbl.loop.if ]
  %pn1 = phi i32* [ %a, %lbl.entry ], [ %pn2, %lbl.loop.if ]
  br i1 %cond2, label %lbl.loop.if, label %lbl.exit

; CHECK: lbl.loop.if:
lbl.loop.if:
; CHECK-DAG: %pn2 = phi i32* [ %b, %lbl.entry ], [ %pn1, %lbl.loop ]
; CHECK-DAG: [[PHI2:%.*]] = phi i32 [ %s2, %lbl.entry ], [ [[PHI1]], %lbl.loop ]
  %pn2 = phi i32* [ %b, %lbl.entry ], [ %pn1, %lbl.loop ]
  br i1 %cond2, label %lbl.loop, label %lbl.exit

; CHECK: lbl.exit:
lbl.exit:
; CHECK-DAG: %arr = phi i32* [ %pn1, %lbl.loop ], [ %pn2, %lbl.loop.if ]
; CHECK-DAG: [[PHI3:%.*]] = phi i32 [ [[PHI1]], %lbl.loop ], [ [[PHI2]], %lbl.loop.if ]
  %arr = phi i32* [ %pn1, %lbl.loop ], [ %pn2, %lbl.loop.if ]
; CHECK: call void @__coco_check_bounds({{.*}}i32 %off, i32 [[PHI3]]{{.*}})
; CHECK: %gep = getelementptr i32, i32* %arr, i32 %off
  %gep = getelementptr i32, i32* %arr, i32 %off
  store i32 1, i32* %gep
  ret void
}


