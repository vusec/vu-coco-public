; CHECK: @g{{.*}}(i32* %a, i32* %b, i32 %off1, i32 %off2, i32 [[SARG1:%.*]], i32 [[SARG2:%.*]])
define void @g(i32* %a, i32* %b, i32 %off1, i32 %off2) {
; CHECK: call void @__coco_check_bounds({{.*}}i32 %off1, i32 [[SARG1]]{{.*}})
; CHECK: %a.idx = getelementptr i32, i32* %a, i32 %off1
  %a.idx = getelementptr i32, i32* %a, i32 %off1
  store i32 1337, i32* %a.idx
; CHECK: call void @__coco_check_bounds({{.*}}i32 %off2, i32 [[SARG2]]{{.*}})
; CHECK: %b.idx = getelementptr i32, i32* %b, i32 %off2
  %b.idx = getelementptr i32, i32* %b, i32 %off2
  store i32 1337, i32* %b.idx
  ret void
}
; CHECK: @f
define void @f(i32 %s) {
; CHECK: %arr1 = alloca i32, i32 %s
; CHECK: %arr2 = alloca i32, i32 10
  %arr1 = alloca i32, i32 %s
  %arr2 = alloca i32, i32 10
; CHECK: call void @g{{.*}}(i32* %arr1, i32* %arr2, i32 100, i32 150, i32 %s, i32 10)
  call void @g(i32* %arr1, i32* %arr2, i32 100, i32 150)
  ret void
}
