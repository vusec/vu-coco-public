; CHECK: @main
define i32 @main(i32 %argc, i8** %argv) {
entry:
; CHECK: %argv.1.ptr = getelementptr i8*, i8** %argv, i32 1
; Bonus might insert boundscheck here
  %argv.1.ptr = getelementptr i8*, i8** %argv, i32 1
; CHECK: %argv.1 = load i8*, i8** %argv.1.ptr
  %argv.1 = load i8*, i8** %argv.1.ptr
; CHECK: %argv.1.3.ptr = getelementptr i8, i8* %argv.1, i32 3
; CHECK-NOT: call
  %argv.1.3.ptr = getelementptr i8, i8* %argv.1, i32 3
  store i8 1, i8* %argv.1.3.ptr
  ret i32 0
}

