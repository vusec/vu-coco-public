@.str = unnamed_addr constant [5 x i8] c"\00foo\00"

; CHECK: @f
define void @f(i32 %o) {
; CHECK-NOT: call
; CHECK:  %.str.1 = getelementptr [5 x i8], [5 x i8]* @.str, i32 0, i32 0
  %.str.1 = getelementptr [5 x i8], [5 x i8]* @.str, i32 0, i32 0

; CHECK: call void @__coco_check_bounds({{.*}}i32 2, i32 5{{.*}})
; CHECK: %.str.idx = getelementptr i8, i8* %.str.1, i32 2
  %.str.idx = getelementptr i8, i8* %.str.1, i32 2
  store i8 1, i8* %.str.idx

; CHECK: call void @__coco_check_bounds({{.*}}i32 9, i32 5{{.*}})
; CHECK: %.str.idx2 = getelementptr i8, i8* %.str.1, i32 9
  %.str.idx2 = getelementptr i8, i8* %.str.1, i32 9
  store i8 1, i8* %.str.idx2
  ret void
}