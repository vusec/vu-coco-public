; CHECK: @main
define i32 @main(i32 %argc, i8** %argv) {
entry:
; CHECK: [[SIZE:%.*]] = add i32 {{.*}}%argc{{.*}}
; CHECK: call void @__coco_check_bounds({{.*}}i32 1, i32 [[SIZE]]{{.*}})
; CHECK: %argv.1.ptr = getelementptr i8*, i8** %argv, i32 1
  %argv.1.ptr = getelementptr i8*, i8** %argv, i32 1
; CHECK: %argv.1 = load i8*, i8** %argv.1.ptr
  %argv.1 = load i8*, i8** %argv.1.ptr
; CHECK: call
; CHECK: %argv.1.3.ptr = getelementptr i8, i8* %argv.1, i32 3
  %argv.1.3.ptr = getelementptr i8, i8* %argv.1, i32 3
  store i8 1, i8* %argv.1.3.ptr
  ret i32 0
}

