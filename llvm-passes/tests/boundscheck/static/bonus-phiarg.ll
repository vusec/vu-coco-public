; CHECK: @f{{.*}}(i1 %cond, i32* %a, i32* %b, i32 %off, i32 [[SARG1:%.*]], i32 [[SARG2:%.*]])
define void @f(i1 %cond, i32* %a, i32* %b, i32 %off) {
  br i1 %cond, label %lbl.if, label %lbl.else

lbl.if:
  br label %lbl.exit
lbl.else:
  br label %lbl.exit

; CHECK: lbl.exit
lbl.exit:
; CHECK-DAG: %arr = phi i32* [ %a, %lbl.if ], [ %b, %lbl.else ]
; CHECK-DAG: [[PHI:%.*]] = phi i32 [ [[SARG1]], %lbl.if ], [ [[SARG2]], %lbl.else ]
  %arr = phi i32* [ %a, %lbl.if ], [ %b, %lbl.else ]
; CHECK: call void @__coco_check_bounds({{.*}}i32 %off, i32 [[PHI]]{{.*}})
; CHECK: %gep = getelementptr i32, i32* %arr, i32 %off
  %gep = getelementptr i32, i32* %arr, i32 %off
  store i32 1, i32* %gep
  ret void
}


