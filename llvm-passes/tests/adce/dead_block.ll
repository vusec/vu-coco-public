; ModuleID = '<string>'
; This file crashes the ADCE pass when not checking for dead blocks when removing dead instructions.
source_filename = "<string>"
target triple = "x86_64-apple-darwin17.3.0"

define i32 @main(i32 %param.argc, i8** %argv) {
entry:
  %argc = alloca i32
  store i32 %param.argc, i32* %argc
  %a = alloca i32
  store i32 1, i32* %a
  %c = alloca i32
  store i32 2, i32* %c
  br i1 true, label %entry.if, label %entry.else

entry.if:                                         ; preds = %entry
  %b = alloca i32
  store i32 1, i32* %b
  %b.1 = load i32, i32* %b
  %.9 = add i32 1, %b.1
  store i32 %.9, i32* %a
  ret i32 1

post_return:                                      ; No predecessors!
  %g = alloca i32
  %c.1 = load i32, i32* %c
  %.12 = add i32 %c.1, 2
  store i32 %.12, i32* %g
  br label %entry.endif

entry.else:                                       ; preds = %entry
  %b.2 = alloca i32
  store i32 2, i32* %b.2
  %b.3 = load i32, i32* %b.2
  %.16 = add i32 %b.3, 1
  store i32 %.16, i32* %a
  ret i32 1

post_return.1:                                    ; No predecessors!
  %g.1 = alloca i32
  %c.2 = load i32, i32* %c
  %.19 = add i32 %c.2, 3
  store i32 %.19, i32* %g.1
  br label %entry.endif

entry.endif:                                      ; preds = %post_return.1, %post_return
  %d = alloca i32
  %a.1 = load i32, i32* %a
  store i32 %a.1, i32* %d
  %c.3 = load i32, i32* %c
  ;CHECK: ret i32
  ret i32 %c.3
}
