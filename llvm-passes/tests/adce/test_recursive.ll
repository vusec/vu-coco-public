; RUN opt  < %s -load ../../llvm-passes/obj/libllvm-passes.so -coco-adce  -S | FileCheck %s

; CHECK: @main
define i32 @main() #0 {
; CHECK-NOT: %1 = add i32 1, 1
  %1 = add i32 1, 1
; CHECK-NOT: %2 = add i32 %1, 1
  %2 = add i32 %1, 1
; CHECK-NOT: %3 = add i32 %2, 1
  %3 = add i32 %2, 1
; CHECK-NOT: %4 = add i32 %3, 1
  %4 = add i32 %3, 1
; CHECK-NOT: %5 = add i32 %4, 1
  %5 = add i32 %4, 1
; CHECK-NOT: %6 = add i32 %5, 1
  %6 = add i32 %5, 1
; CHECK-NOT: %7 = add i32 %6, 1
  %7 = add i32 %6, 1
; CHECK-NOT: %8 = add i32 %7, 1
  %8 = add i32 %7, 1
  ret i32 0
}