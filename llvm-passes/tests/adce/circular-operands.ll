; This pass will fail if the adce pass re-adds already maked live instructions
; in instruction operands. This can happen if there's a ciruclar dependency of
; the instructions as below for %i.0 and %.9.

define i32 @main() {
entry:
  br label %entry.while.cond

entry.while.cond:                                 ; preds = %entry.while.body, %entry
  %i.0 = phi i32 [ 0, %entry ], [ %.9, %entry.while.body ]
  %.7 = icmp slt i32 %i.0, 10
  br i1 %.7, label %entry.while.body, label %entry.endwhile

entry.while.body:                                 ; preds = %entry.while.cond
  %.9 = add i32 %i.0, 1
  br label %entry.while.cond

entry.endwhile:                                   ; preds = %entry.while.cond
  ; CHECK: ret i32 1
  ret i32 1
}
